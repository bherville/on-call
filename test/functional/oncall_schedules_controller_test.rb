require 'test_helper'

class OncallSchedulesControllerTest < ActionController::TestCase
  setup do
    @oncall_schedule = oncall_schedules(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:oncall_schedules)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create oncall_schedule" do
    assert_difference('OncallSchedule.count') do
      post :create, oncall_schedule: { end_date: @oncall_schedule.end_date, start_date: @oncall_schedule.start_date }
    end

    assert_redirected_to oncall_schedule_path(assigns(:oncall_schedule))
  end

  test "should show oncall_schedule" do
    get :show, id: @oncall_schedule
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @oncall_schedule
    assert_response :success
  end

  test "should update oncall_schedule" do
    put :update, id: @oncall_schedule, oncall_schedule: { end_date: @oncall_schedule.end_date, start_date: @oncall_schedule.start_date }
    assert_redirected_to oncall_schedule_path(assigns(:oncall_schedule))
  end

  test "should destroy oncall_schedule" do
    assert_difference('OncallSchedule.count', -1) do
      delete :destroy, id: @oncall_schedule
    end

    assert_redirected_to oncall_schedules_path
  end
end
