require 'test_helper'

class ManualOncallScheduleApprovalsControllerTest < ActionController::TestCase
  setup do
    @manual_oncall_schedule_approval = manual_oncall_schedule_approvals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:manual_oncall_schedule_approvals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create manual_oncall_schedule_approval" do
    assert_difference('ManualOncallScheduleApproval.count') do
      post :create, manual_oncall_schedule_approval: { admin_approval: @manual_oncall_schedule_approval.admin_approval, admin_user_id: @manual_oncall_schedule_approval.admin_user_id, manial_oncall_schedule_id: @manual_oncall_schedule_approval.manial_oncall_schedule_id, oncall_user_approval: @manual_oncall_schedule_approval.oncall_user_approval, overridden_on_call_user_approval: @manual_oncall_schedule_approval.overridden_on_call_user_approval }
    end

    assert_redirected_to manual_oncall_schedule_approval_path(assigns(:manual_oncall_schedule_approval))
  end

  test "should show manual_oncall_schedule_approval" do
    get :show, id: @manual_oncall_schedule_approval
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @manual_oncall_schedule_approval
    assert_response :success
  end

  test "should update manual_oncall_schedule_approval" do
    put :update, id: @manual_oncall_schedule_approval, manual_oncall_schedule_approval: { admin_approval: @manual_oncall_schedule_approval.admin_approval, admin_user_id: @manual_oncall_schedule_approval.admin_user_id, manial_oncall_schedule_id: @manual_oncall_schedule_approval.manial_oncall_schedule_id, oncall_user_approval: @manual_oncall_schedule_approval.oncall_user_approval, overridden_on_call_user_approval: @manual_oncall_schedule_approval.overridden_on_call_user_approval }
    assert_redirected_to manual_oncall_schedule_approval_path(assigns(:manual_oncall_schedule_approval))
  end

  test "should destroy manual_oncall_schedule_approval" do
    assert_difference('ManualOncallScheduleApproval.count', -1) do
      delete :destroy, id: @manual_oncall_schedule_approval
    end

    assert_redirected_to manual_oncall_schedule_approvals_path
  end
end
