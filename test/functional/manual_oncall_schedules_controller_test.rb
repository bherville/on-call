require 'test_helper'

class ManualOncallSchedulesControllerTest < ActionController::TestCase
  setup do
    @manual_oncall_schedule = manual_oncall_schedules(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:manual_oncall_schedules)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create manual_oncall_schedule" do
    assert_difference('ManualOncallSchedule.count') do
      post :create, manual_oncall_schedule: { created_by: @manual_oncall_schedule.created_by, end_date: @manual_oncall_schedule.end_date, oncall_schedule_id: @manual_oncall_schedule.oncall_schedule_id, oncall_user_id: @manual_oncall_schedule.oncall_user_id, overridden_on_call_user_id: @manual_oncall_schedule.overridden_on_call_user_id, start_date: @manual_oncall_schedule.start_date }
    end

    assert_redirected_to manual_oncall_schedule_path(assigns(:manual_oncall_schedule))
  end

  test "should show manual_oncall_schedule" do
    get :show, id: @manual_oncall_schedule
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @manual_oncall_schedule
    assert_response :success
  end

  test "should update manual_oncall_schedule" do
    put :update, id: @manual_oncall_schedule, manual_oncall_schedule: { created_by: @manual_oncall_schedule.created_by, end_date: @manual_oncall_schedule.end_date, oncall_schedule_id: @manual_oncall_schedule.oncall_schedule_id, oncall_user_id: @manual_oncall_schedule.oncall_user_id, overridden_on_call_user_id: @manual_oncall_schedule.overridden_on_call_user_id, start_date: @manual_oncall_schedule.start_date }
    assert_redirected_to manual_oncall_schedule_path(assigns(:manual_oncall_schedule))
  end

  test "should destroy manual_oncall_schedule" do
    assert_difference('ManualOncallSchedule.count', -1) do
      delete :destroy, id: @manual_oncall_schedule
    end

    assert_redirected_to manual_oncall_schedules_path
  end
end
