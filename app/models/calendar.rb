class Calendar < ActiveRecord::Base
  attr_accessible :oncall_schedule_id, :overwrite_user_id

  belongs_to :oncall_schedule
  belongs_to :last_modified_by, :class_name => 'User', :foreign_key => 'last_modified_by'

  def self.get_calendar
    calendar = Calendar.first
    calendar
  end

  def schedule_overwritten
    ManualOncallSchedule.get_manual_oncall_schedule(Date.today, self.oncall_schedule)
  end

  def send_schedule_overwritten_to_admins
    send_schedule_overwritten User.select { |u| u.has_role? :admin }
  end

  def send_schedule_overwritten_to_users(skip_admins = false)
    if skip_admins
      users = self.oncall_schedule.oncall_profiles.map { |profile| profile.user }.select { |u| !u.has_role? :admin }
    else
      users = self.oncall_schedule.oncall_profiles.map { |profile| profile.user }
    end

    send_schedule_overwritten users
    self.overwrite_user_notification_sent = true
    self.save
  end

  def send_schedule_overwritten_to_mailing_list
    send_schedule_overwritten self.oncall_schedule.mailing_list
    self.overwrite_user_notification_sent = true
    self.save
  end

  private
  def send_schedule_overwritten(resources, skip_admins = false)
    if self.oncall_schedule.present?
      resources.each do |resource|
        if resource.class.name == 'User'
          OncallScheduleNotifier.delay.on_call_overwritten(resource, self.overwrite_user)
        else
          OncallScheduleNotifier.delay.on_call_overwritten_mailing_list(resource, self.overwrite_user)
        end
      end
    end
  end
end
