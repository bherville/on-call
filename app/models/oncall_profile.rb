class OncallProfile < ActiveRecord::Base
  attr_accessible :oncall_position, :user, :user_id, :oncall_schedule_id, :oncall_schedule

  validates :user_id, presence: true
  validates_uniqueness_of :oncall_position, :scope => :oncall_schedule_id

  belongs_to :user
  belongs_to :oncall_schedule
end
