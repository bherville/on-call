require 'role_model'

class User < ActiveRecord::Base
  include Gravtastic
  gravtastic :secure => false

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :lockable, :async

  devise :omniauthable, :omniauth_providers => [:auth_man]

  include RoleModel

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  attr_accessible :time_zone, :roles, :roles_mask, :fname, :lname, :pager_address,
                  :notify, :notifications

  # optionally set the integer attribute to store the roles in,
  # :roles_mask is the default
  roles_attribute :roles_mask

  # declare the valid roles -- do not change the order if you add more
  # roles later, always append them at the end!
  roles :admin, :scheduler, :user

  bitmask :notifications, :as => [:oncall_reminder_email, :oncall_reminder_pager, :oncall_monthly]

  has_many :oncall_profiles
  has_many :oncall_schedules, through: :oncall_profiles
  has_many :manual_oncall_schedules
  has_many :manual_oncall_schedule_approvals
  has_one :calendar

  validates :fname, :presence => true
  validates :lname, :presence => true
  validates :pager_address, :presence => true

  before_destroy :destroy_profiles

  def password_required?
    super if confirmed?
  end

  def password_match?
    self.errors[:password] << "can't be blank" if password.blank?
    self.errors[:password_confirmation] << "can't be blank" if password_confirmation.blank?
    self.errors[:password_confirmation] << "does not match password" if password != password_confirmation
    password == password_confirmation && !password.blank?
  end

  def full_name
    self.fname && self.lname ? "#{self.fname} #{self.lname}" : nil
  end

  def full_name_comma
    self.fname && self.lname ? "#{self.lname}, #{self.fname}" : nil
  end

  def name
    self.fname && self.lname ? "#{self.fname} #{self.lname}" : self.email
  end

  def self.from_omniauth(auth)
    user_return = where(provider: auth.provider, uid: auth.uid).first

    unless user_return
      user = User.find_by_email(auth.info['email'])

      user ||= User.new(
          :email        => auth.info.email,
          :password     => Devise.friendly_token[0,20],
          :fname        => auth.info.fname,
          :lname        => auth.info.lname,
          :time_zone    => auth.info.time_zone,
      )

      user.pager_address ||= '5555555555@vtext.com'
      user.uid      = auth.uid
      user.provider = auth.provider

      user.skip_confirmation!
      user.save

      user_return = user
    end

    user_return.sync_with_omniauth(auth)

    user_return
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session['devise.auth_man_data'] && session['devise.auth_man_data']['extra']['raw_info']
        user.email      = data['email'] if user.email.blank?
        user.fname      = data['fname'] if user.fname.blank?
        user.lname      = data['lname'] if user.lname.blank?
        user.time_zone  = data['time_zone'] if user.time_zone.blank?
      end
    end
  end

  def sync_with_omniauth(auth)
    unless self.confirmed?
      self.skip_confirmation!

      self.save
    end

    unless auth.info.fname == self.fname && auth.info.lname == self.lname && auth.info.time_zone == self.time_zone
      self.fname      = auth.info.fname
      self.lname      = auth.info.lname
      self.time_zone  = auth.info.time_zone

      self.save
    end
  end

  private
  def destroy_profiles
    self.oncall_profiles.each do |profile|
      profile.destroy
    end
  end
end
