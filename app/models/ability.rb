class Ability
  include CanCan::Ability

  def initialize(user)
    if user.nil?
      user = User.new
      current_user = user
    end
    models = [Calendar, User]

    if user.has_role? :admin
      can :manage, [User, Admin]
      can :read, models
    end

    if user.has_role? :scheduler
      can :manage, [OncallSchedule, Calendar, ManualOncallSchedule, ManualOncallScheduleApproval]
      can :read, models
    end

    if user.has_role? :user
      can [:read, :create], [ManualOncallSchedule]
      can [:approve, :reject], [ManualOncallScheduleApproval]
      can [:read, :edit], User
    end

    can :read, Calendar
  end
end
