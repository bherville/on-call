class ManualOncallSchedule < ActiveRecord::Base
  attr_accessible :created_by, :end_date, :oncall_schedule_id, :oncall_user_id, :overridden_on_call_user_id, :start_date, :approval

  belongs_to :oncall_user, :foreign_key => :oncall_user_id, :class_name => 'User'
  belongs_to :overridden_on_call_user, :foreign_key => :overridden_on_call_user_id, :class_name => 'User'
  belongs_to :created_by, :foreign_key => :created_by, :class_name => 'User'
  belongs_to :oncall_schedule

  has_one :approval, :class_name => ManualOncallScheduleApproval, :foreign_key => :manual_oncall_schedule_id, :dependent => :delete

  validate :validate_manual_oncall_schedule
  validate :valid_dates
  validate :valid_override_users

  after_create :create_approval
  before_save :flatten_override_user

  def self.get_manual_oncall_schedule(date, oncall_schedule)
    manual_schedule = nil

    oncall_schedule.manual_oncall_schedules.each do |schedule|
      next unless date.between?(schedule.start_date, schedule.end_date)

      manual_schedule = schedule if schedule.valid_schedule?
    end

    manual_schedule
  end

  def valid_schedule?
    self.approval.approved?
  end

  private
  def valid_manual_oncall_schedule?
    start_manual_schedule = ManualOncallSchedule.get_manual_oncall_schedule self.start_date, self.oncall_schedule
    end_manual_schedule = ManualOncallSchedule.get_manual_oncall_schedule self.end_date, self.oncall_schedule

    if (start_manual_schedule && end_manual_schedule) && (start_manual_schedule.approval.rejected? && end_manual_schedule.approval.rejected?)
      true
    elsif start_manual_schedule.nil? && end_manual_schedule.nil?
      true
    else
      false
    end
  end

  def validate_manual_oncall_schedule
    errors.add(:manual_oncall_schedule, "is invalid. The start or end date you have entered is between a start and end date of an existing override.") unless valid_manual_oncall_schedule?
  end

  def valid_dates
    if self.start_date <= self.end_date
      true
    else
      errors.add(:manual_oncall_schedule, "is invalid. The start date must come before the end date.")
      false
    end
  end

  def create_approval
    self.approval ||= ManualOncallScheduleApproval.new(:oncall_user_approval => true)
  end

  def valid_override_users
    valid_users = self.oncall_schedule.users_between_dates(self.start_date, self.end_date)

    if valid_users.empty?
      errors.add(:manual_oncall_schedule, "date range is invalid. The date range you chose does not fall within anyone's scheduled on call duties.")
      false
    else
      if valid_users.count == 1
        if valid_users.first[:oncall_profile].user == self.oncall_user
          errors.add(:manual_oncall_schedule, "date range is invalid. You are attempting to override yourself.")
          false
        else
          true
        end
      elsif valid_users.count > 1
        errors.add(:manual_oncall_schedule, "date range is invalid. This override would affect more than one user, you can only override one users schedule at a time..")
        false
      end
    end
  end

  def flatten_override_user
    self.overridden_on_call_user = self.oncall_schedule.users_between_dates(self.start_date, self.end_date).first[:oncall_profile].user
  end
end