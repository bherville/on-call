class OncallSchedule < ActiveRecord::Base
  attr_accessible :end_date, :start_date, :oncall_profiles, :oncall_profiles_attributes,
                  :mailing_list, :weeks, :name, :oncall_start_time, :oncall_start_day

  validates :end_date, presence: true, :unless => :weeks
  validates :start_date, presence: true
  validates :name, presence: true, :uniqueness => true
  validates :oncall_start_time, presence: true
  validates :oncall_start_day, presence: true

  belongs_to :scheduler, :class_name => 'User', :foreign_key => 'scheduler_id'
  has_many :calendars
  has_many :oncall_profiles
  belongs_to :last_notification_sent_to, :class_name => 'User', :foreign_key => 'last_notification_sent_to'
  has_many :manual_oncall_schedules
  has_many :users, :through => :oncall_profiles

  accepts_nested_attributes_for :oncall_profiles, :allow_destroy => true

  before_destroy :destroy_profiles
  before_save :mailing_list_to_array

  serialize :mailing_list

  def scheduled_users
    self.oncall_profiles.select { |p| p.oncall_position }.map{|p| p.user if p.oncall_position}
  end

  def profiles_to_schedule
    profiles = []

    start_week = self.start_date.beginning_of_week(start_day = self.oncall_start_day.downcase.to_sym)
    end_week = start_week + 1.weeks

    weeks_to_process = self.weeks ? self.weeks : (self.end_date.beginning_of_week - self.start_date.beginning_of_week(start_day = self.oncall_start_day.downcase.to_sym))/1.week

    valid_profiles = self.oncall_profiles.select { |p| p.oncall_position }
    sorted_profiles = valid_profiles.sort! { |x, y| x.oncall_position <=> y.oncall_position }
    weeks_to_process.to_int.times do
      sorted_profiles.each do |profile|
        break if profile.oncall_position.nil?

        if self.weeks
          break if start_week >= self.start_date.beginning_of_week(start_day = self.oncall_start_day.downcase.to_sym) + self.weeks.weeks
        else
          break if start_week > self.end_date.beginning_of_week
        end

        profiles << { :oncall_profile => profile, :start_date => start_week, :end_date => (end_week - 1.day) }

        start_week = end_week
        end_week = start_week + 1.weeks
      end
    end

    profiles
  end

  def users_between_dates(start_date, end_date)
    profiles = profiles_to_schedule

    valid_users = Array.new

    profiles.each do |p|
      valid_start = start_date.to_datetime.between? p[:start_date], p[:end_date]
      valid_stop = end_date.to_datetime.between? p[:start_date], p[:end_date]

      valid_users << p if valid_start || valid_stop
    end

    valid_users
  end

  def month_on_call
    profiles = self.profiles_to_schedule

    start_of_month = Date.today.beginning_of_month
    end_of_month = Date.today.end_of_month
    valid_profiles = Array.new
    profiles.each do |profile|
      if (start_of_month..end_of_month).cover?(Date.parse(profile[:start_date].time.strftime('%Y/%m/%d')))
        valid_profiles << profile
      end
    end

    valid_profiles
  end

  def current_on_call
    profiles = self.profiles_to_schedule

    start_of_week = Time.zone.now.beginning_of_week(self.oncall_start_day.downcase.to_sym)
    end_of_week = Time.zone.now.end_of_week

    start_of_last_week = (Time.zone.now - 6.days).beginning_of_week(self.oncall_start_day.downcase.to_sym)
    end_of_last_week = start_of_week
    profiles.each do |profile|
      if (start_of_week..end_of_week).cover?(profile[:start_date])
        now = Time.now.in_time_zone
        day_numbers = Hash[Date::DAYNAMES.map.with_index.to_a]
        scheduled_day = day_numbers[self.oncall_start_day]
        now_day = now.wday

        if (now_day == scheduled_day && now.strftime( "%H%M%S%N" ) >= self.oncall_start_time.in_time_zone.strftime( "%H%M%S%N" )) ||  now_day > scheduled_day
          return profile[:oncall_profile]
        end
      end
    end

    profiles.each do |profile|
      if (start_of_last_week..end_of_last_week).cover?(profile[:start_date])# && (start_of_last_week..end_of_last_week).cover?(profile[:end_date])
        return profile[:oncall_profile]
      end
    end

    nil
  end

  def notify_on_call(force = false)
    on_call = self.current_on_call.user

    if (self.last_notification_sent_to != on_call || force) || self.oncall_profiles.count == 1
      OncallScheduleNotifier.delay.on_call_reminder(on_call) if on_call.notifications?(:oncall_reminder_email)
      OncallScheduleNotifier.delay.on_call_reminder_pager(on_call) if on_call.notifications?(:oncall_reminder_pager)

      self.last_notification_sent_to = on_call
      self.save!
    end
  end

  def notify_all_on_call(force = false)
    if self.send_notify? || force
      on_call = Calendar.get_calendar.overwrite_user
      on_call ||= self.current_on_call.user

      self.oncall_profiles.each do |profile|
        OncallScheduleNotifier.delay.on_call_all_reminder(profile.user, on_call) if profile.user.notifications?(:oncall_reminder_email)
      end

      self.mailing_list.each do |email|
        OncallScheduleNotifier.delay.on_call_reminder_mailing_list(email, on_call)
      end

      self.last_notification_sent_to = on_call
      self.save!
    end
  end

  def send_notify?
    on_call = self.current_on_call.user
    self.last_notification_sent_to != on_call || self.oncall_profiles.count == 1
  end

  def send_monthly_notify?
    today = Date.today

    self.last_monthly_notification_sent_at.nil? ||
        (self.last_monthly_notification_sent_at.month < today.month && self.last_monthly_notification_sent_at.year <= today.year)
  end

  def notify_all_monthly_on_call
    on_calls = self.month_on_call

    if self.oncall_profiles.count > 0
      self.oncall_profiles.each do |profile|
        OncallScheduleNotifier.delay.on_call_monthly(profile.user, on_calls) if profile.user.notifications?(:oncall_monthly)
      end

      self.mailing_list.each do |email|
        OncallScheduleNotifier.delay.on_call_monthly_mailing_list(email, on_calls)
      end

      self.last_monthly_notification_sent_at = Date.today

      self.save
    end
  end

  private
  def destroy_profiles
    self.oncall_profiles.each do |profile|
      profile.destroy
    end
  end

  def mailing_list_to_array
    if self.mailing_list.is_a? String
      self.mailing_list = self.mailing_list.split("\r\n")
      self.mailing_list.each do |email|
        email.strip
      end
    end
  end
end