class ManualOncallScheduleApproval < ActiveRecord::Base
  attr_accessible :admin_approval, :admin_user_id, :manual_oncall_schedule_id, :oncall_user_approval, :overridden_on_call_user_approval, :admin_reject, :oncall_user_reject, :overridden_on_call_user_reject

  belongs_to :manual_oncall_schedule
  belongs_to :admin_user, :foreign_key => :id, :class_name => 'User'

  validates :manual_oncall_schedule_id, :presence => true,
                                        :uniqueness => true

  after_create :notify_users


  def approved?
    (self.oncall_user_approval? && self.overridden_on_call_user_approval?) || self.admin_approval?
  end

  def rejected?
    (self.oncall_user_reject? && self.overridden_on_call_user_reject?) || self.admin_reject?
  end


  def admin_approved?
    self.admin_approval?
  end

  def admin_rejected?
    self.admin_reject?
  end

  def approve
    if self.approved?
      errors.add(:manual_oncall_schedule_approval, "is already approved.")

      false
    else
      self.overridden_on_call_user_approval = true
      self.oncall_user_reject = false
      self.overridden_on_call_user_reject = false
      self.save

      true
    end
  end

  def reject
    if self.rejected?
      errors.add(:manual_oncall_schedule_approval, "is already rejected.")

      false
    else
      self.oncall_user_reject = true
      self.overridden_on_call_user_reject = true
      self.overridden_on_call_user_approval = false
      self.save

      true
    end
  end

  def admin_approve
    if self.admin_approved?
      errors.add(:manual_oncall_schedule_approval, "is already approved.")

      false
    else
      self.oncall_user_approval = true
      self.overridden_on_call_user_approval = true
      self.admin_approval = true

      self.oncall_user_reject = false
      self.overridden_on_call_user_reject = false
      self.admin_reject = false
      self.save
      true
    end
  end

  def admin_reject
    if self.admin_rejected?
      errors.add(:manual_oncall_schedule_approval, "is already rejected.")

      false
    else
      self.oncall_user_reject = true
      self.overridden_on_call_user_reject = true
      self.admin_reject = true

      self.overridden_on_call_user_approval = false
      self.admin_approval = false
      self.save
      true
    end
  end

  def notify_users_status(admin_change = false)
    if self.approved?
      # Send Approval Update
      ManualOncallScheduleApprovalNotifier.on_call_override_approved(self.manual_oncall_schedule.oncall_user, self.manual_oncall_schedule.overridden_on_call_user, self, self.manual_oncall_schedule.id).deliver
    elsif self.rejected?
      # Send Rejection Update
      ManualOncallScheduleApprovalNotifier.on_call_override_rejected(self.manual_oncall_schedule.oncall_user, self.manual_oncall_schedule.overridden_on_call_user, self, self.manual_oncall_schedule.id).deliver
    else
      #This is probably being called on create
      ManualOncallScheduleApprovalNotifier.on_call_override_approval(self.manual_oncall_schedule.overridden_on_call_user, self.manual_oncall_schedule.oncall_user, self, self.manual_oncall_schedule.id).deliver
    end

    # Send Status Update to requesting user
    #ManualOncallScheduleApprovalNotifier.on_call_override_status(self.manual_oncall_schedule.oncall_user, self.manual_oncall_schedule.overridden_on_call_user, self, self.manual_oncall_schedule.id).deliver
    # Send status update to the overriding user
    #ManualOncallScheduleApprovalNotifier.on_call_override_status(self.manual_oncall_schedule.overridden_on_call_user, self.manual_oncall_schedule.overridden_on_call_user, self, self.manual_oncall_schedule.id).deliver

    # Send email to all schedulers
    self.manual_oncall_schedule.oncall_schedule.users.each do |u|
      ManualOncallScheduleApprovalNotifier.on_call_override_status(u, self.manual_oncall_schedule.overridden_on_call_user, self, self.manual_oncall_schedule.id).deliver if (u.has_role?(:scheduler) && u != self.manual_oncall_schedule.oncall_user)
    end
  end

  def status_string
    if self.admin_rejected?
      I18n.t('manual_oncall_schedule_approvals.admin_rejected')
    elsif self.admin_approved?
      I18n.t('manual_oncall_schedule_approvals.admin_approved')
    elsif self.rejected?
      I18n.t('manual_oncall_schedule_approvals.rejected')
    else
      self.approved? ? I18n.t('manual_oncall_schedule_approvals.approved') : I18n.t('manual_oncall_schedule_approvals.pending_approval')
    end
  end

  private
  def notify_users
    self.delay.notify_users_status
  end
end
