class NotifierObserver < ActiveRecord::Observer
  observe :calendar

  def after_update(record)
    if record.is_a? Calendar
      return #Disabled
      admin_users = User.select { |u| u.has_role? :admin }

      admin_users.each do |user|
        OncallScheduleNotifier.delay.schedule_change(user, record.last_modified_by)
      end

      record.oncall_schedule.oncall_profiles.map { |profile| profile.user }.each do |user|
        OncallScheduleNotifier.delay.schedule_change(user, record.last_modified_by) unless admin_users.include?(user)
      end

      record.oncall_schedule.mailing_list.each do |email|
        OncallScheduleNotifier.delay.schedule_change_mailing_list(email, record.last_modified_by)
      end
    end
  end
end
