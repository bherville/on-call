class ManualOncallScheduleApprovalNotifier < ActionMailer::Base
  default from: APP_CONFIG['smtp']['from_address']

  def on_call_override_approval(user, requesting_user, approval, manual_oncall_schedule_id)
    @user = user
    @requesting_user = requesting_user
    @approval = approval
    @manual_oncall_schedule = ManualOncallSchedule.find(manual_oncall_schedule_id)
    @approve_url = approve_manual_oncall_schedule_approval_url(@approval)
    @reject_url = reject_manual_oncall_schedule_approval_url(@approval)
    @override_start_date = @manual_oncall_schedule.start_date
    @override_end_date = @manual_oncall_schedule.end_date
    @oncall_user = @manual_oncall_schedule.oncall_user
    @approval_status = @approval.status_string

    mail to: @user.email, subject: t('mailers.manual_oncall_schedule_approval_notifier.subject_base') % t('mailers.manual_oncall_schedule_approval_notifier.on_call_override_approval.subject')
  end

  def on_call_override_status(user, overridden_user, approval, manual_oncall_schedule_id)
    @user = user
    @overridden_user = overridden_user
    @approval = approval
    @manual_oncall_schedule = ManualOncallSchedule.find(manual_oncall_schedule_id)
    @override_start_date = @manual_oncall_schedule.start_date
    @override_end_date = @manual_oncall_schedule.end_date
    @oncall_user = @manual_oncall_schedule.oncall_user
    @approval_status = @approval.status_string

    mail to: @user.email, subject: t('mailers.manual_oncall_schedule_approval_notifier.subject_base') % t('mailers.manual_oncall_schedule_approval_notifier.on_call_override_approval.subject')
  end

  def on_call_override_approved(user, overridden_user, approval, manual_oncall_schedule_id)
    @user = user
    @overridden_user = overridden_user
    @approval = approval
    @manual_oncall_schedule = ManualOncallSchedule.find(manual_oncall_schedule_id)
    @override_start_date = @manual_oncall_schedule.start_date
    @override_end_date = @manual_oncall_schedule.end_date
    @oncall_user = @manual_oncall_schedule.oncall_user
    @approval_status = @approval.status_string

    mail to: @user.email, subject: t('mailers.manual_oncall_schedule_approval_notifier.subject_base') % t('mailers.manual_oncall_schedule_approval_notifier.on_call_override_approved.subject')
  end

  def on_call_override_rejected(user, overridden_user, approval, manual_oncall_schedule_id)
    @user = user
    @overridden_user = overridden_user
    @approval = approval
    @manual_oncall_schedule = ManualOncallSchedule.find(manual_oncall_schedule_id)
    @override_start_date = @manual_oncall_schedule.start_date
    @override_end_date = @manual_oncall_schedule.end_date
    @oncall_user = @manual_oncall_schedule.oncall_user
    @approval_status = @approval.status_string

    mail to: @user.email, subject: t('mailers.manual_oncall_schedule_approval_notifier.subject_base') % t('mailers.manual_oncall_schedule_approval_notifier.on_call_override_rejected.subject')
  end
end
