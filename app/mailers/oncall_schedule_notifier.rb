class OncallScheduleNotifier < ActionMailer::Base
  default from: APP_CONFIG['smtp']['from_address']

  def schedule_change(user, changed_by)
    @user = user
    @changed_by = changed_by

    mail to: @user.email, subject: t('mailers.oncall_schedule_notifier.subject_base') % t('mailers.oncall_schedule_notifier.schedule_change.subject')
  end

  def schedule_change_mailing_list(email, changed_by)
    @email = email
    @changed_by = changed_by

    mail to: @email, subject: t('mailers.oncall_schedule_notifier.subject_base') % t('mailers.oncall_schedule_notifier.schedule_change.subject')
  end

  def on_call_reminder(user)
    @user = user

    mail to: user.email, subject: t('mailers.oncall_schedule_notifier.subject_base') % t('mailers.oncall_schedule_notifier.on_call_reminder.subject')
  end

  def on_call_reminder_pager(user)
    @user = user

    mail to: user.pager_address, subject: t('mailers.oncall_schedule_notifier.subject_base') % t('mailers.oncall_schedule_notifier.on_call_reminder.subject')
  end

  def on_call_all_reminder(user, oncall_user)
    @user = user
    @oncall_user = oncall_user

    mail to: user.email, subject: t('mailers.oncall_schedule_notifier.subject_base') % t('mailers.oncall_schedule_notifier.on_call_reminder.subject')
  end

  def on_call_reminder_mailing_list(email, oncall_user = nil)
    @email = email
    @oncall_user = oncall_user

    mail to: email, subject: t('mailers.oncall_schedule_notifier.subject_base') % t('mailers.oncall_schedule_notifier.on_call_reminder.subject')
  end

  def on_call_monthly(user, profiles)
    @user = user
    @profiles = profiles

    mail to: user.email, subject: t('mailers.oncall_schedule_notifier.subject_base') % t('mailers.oncall_schedule_notifier.on_call_monthly.subject')
  end

  def on_call_monthly_mailing_list(email, profiles)
    @email = email
    @profiles = profiles

    mail to: email, subject: t('mailers.oncall_schedule_notifier.subject_base') % t('mailers.oncall_schedule_notifier.on_call_monthly.subject')
  end

  def on_call_overwritten(user, oncall_user)
    @user = user
    @oncall_user = oncall_user

    mail to: user.email, subject: t('mailers.oncall_schedule_notifier.subject_base') % t('mailers.oncall_schedule_notifier.on_call_overwrite.subject')
  end

  def on_call_overwritten_mailing_list(email, oncall_user)
    @email = email
    @oncall_user = oncall_user

    mail to: email, subject: t('mailers.oncall_schedule_notifier.subject_base') % t('mailers.oncall_schedule_notifier.on_call_overwrite.subject')
  end
end
