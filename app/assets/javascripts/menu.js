function hide_menu_item(dropdown) {
    var $dropdown = $(dropdown);

    if ($dropdown.children('ul').text().trim().length == 0) {
        $dropdown.hide();
    }
}

$(document).ready(function() {
    $('.dropdown-toggle').dropdown();
    hide_menu_item('#admin_dropdown');
    hide_menu_item('#manage_dropdown');
});



