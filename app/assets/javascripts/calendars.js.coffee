# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  if $('#calendar')
    $.ajax '/api/calendars.json',
      type: 'GET'
      error: (jqXHR, textStatus, errorThrown) ->
        console.log(textStatus);
        console.log(errorThrown);
      success: (ajaxdata, textStatus, jqXHR) ->
        $scheduleId = ajaxdata['oncall_schedule_id']
        $('#calendar').fullCalendar({
          events: '/api/oncall_schedules/' + $scheduleId + '.json'
          loading: (data, jsEvent, view) ->
            if data == false
              $(".fc-event:contains('OVERRIDE -')").css("background-color", "red")
        })