# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
  if ($('form[id="new_manual_oncall_schedule"]').length == 1)
    intiaite_override_user_update()

    $('#manual_oncall_schedule_start_date_1i').change ->
      intiaite_override_user_update()

    $('#manual_oncall_schedule_start_date_2i').change ->
      intiaite_override_user_update()

    $('#manual_oncall_schedule_start_date_3i').change ->
      intiaite_override_user_update()

    $('#manual_oncall_schedule_end_date_1i').change ->
      intiaite_override_user_update()

    $('#manual_oncall_schedule_end_date_2i').change ->
      intiaite_override_user_update()

    $('#manual_oncall_schedule_end_date_3i').change ->
      intiaite_override_user_update()

format_date = (year, month, day) ->
  day + '-' + month + '-' + year

format_users_array = (users) ->
  formated_users =  for i in [0...users.length]
    "#{users[i].name}"

intiaite_override_user_update = ->
  override_user_span = 'overridden_on_call_user'
  api_path = '/api/calendars/on_call_users_by_date_range'

  start_date = format_date($('#manual_oncall_schedule_start_date_1i')[0].value, $('#manual_oncall_schedule_start_date_2i')[0].value, $('#manual_oncall_schedule_start_date_3i')[0].value)
  end_date  = format_date($('#manual_oncall_schedule_end_date_1i')[0].value, $('#manual_oncall_schedule_end_date_2i')[0].value, $('#manual_oncall_schedule_end_date_3i')[0].value)

  $.ajax
    url: api_path
    dataType: "json"
    data:
      start_date: start_date
      end_date: end_date
    error: (jqXHR, textStatus, errorThrown) ->
      alert  "AJAX Error: #{textStatus}"
    success: (data, textStatus, jqXHR) ->
      users = format_users_array(data.users)
      $('#' + override_user_span).val(users.join(","))
