# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  $oncall_schedule_no_end_date = '#oncall_schedule_weeks'
  if $("[id^=oncall_schedule_end_date_]").prop("disabled")
    $("[id^=oncall_schedule_end_date_]").prop("disabled", $($oncall_schedule_no_end_date).val().length > 0);

  $($oncall_schedule_no_end_date).bind 'change', (event) =>
    $("[id^=oncall_schedule_end_date_]").prop("disabled", $($oncall_schedule_no_end_date).val().length > 0);
