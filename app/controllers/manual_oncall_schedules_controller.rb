class ManualOncallSchedulesController < ApplicationController
  load_and_authorize_resource

  # GET /manual_oncall_schedules
  def index
    if current_user.has_role? :scheduler
      @manual_oncall_schedules = ManualOncallSchedule.all
    else
      @manual_oncall_schedules.where({oncall_user_id: current_user.id, overridden_on_call_user_id: current_user.id })
      @manual_oncall_schedules ||= Array.new
    end

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /manual_oncall_schedules/1
  def show
    @manual_oncall_schedule = ManualOncallSchedule.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /manual_oncall_schedules/new
  def new
    @manual_oncall_schedule = ManualOncallSchedule.new
    @oncall_schedule = Calendar.get_calendar.oncall_schedule

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # POST /manual_oncall_schedules
  def create
    @oncall_schedule = Calendar.get_calendar.oncall_schedule
    @manual_oncall_schedule = ManualOncallSchedule.new(params[:manual_oncall_schedule])

    @manual_oncall_schedule.created_by = current_user
    @manual_oncall_schedule.oncall_user_id = current_user.id
    @manual_oncall_schedule.oncall_schedule = Calendar.get_calendar.oncall_schedule

    respond_to do |format|
      if @manual_oncall_schedule.save
        format.html { redirect_to @manual_oncall_schedule, notice: 'Manual oncall schedule was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # DELETE /manual_oncall_schedule/1
  def destroy
    @manual_oncall_schedule = ManualOncallSchedule.find(params[:id])
    @manual_oncall_schedule.destroy

    respond_to do |format|
      format.html { redirect_to manual_oncall_schedule_url }
    end
  end
end
