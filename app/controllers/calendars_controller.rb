class CalendarsController < ApplicationController
  load_and_authorize_resource
  before_filter :create_calendar

  # GET /calendars
  def show
    @calendar = Calendar.get_calendar

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /calendars/edit
  def edit
    @calendar = Calendar.first
    p @calendar
  end

  # PUT /calendars/1
  def update
    @calendar = Calendar.get_calendar
    @calendar.last_modified_by = current_user

    respond_to do |format|
      if @calendar.update_attributes(params[:calendar])
        format.html { redirect_to calendars_path, notice: 'Calendar was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  private
  def create_calendar
    if Calendar.count == 0
      Calendar.create
    end
  end
end
