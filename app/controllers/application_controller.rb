class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :set_timezone

  rescue_from CanCan::AccessDenied do |exception|
    if user_signed_in?
      flash[:alert] = t('user.not_authorized')
      session[:user_return_to] = nil

      respond_to do |format|
        format.html { redirect_to root_url }
        format.json { render json: { :message => flash[:error], :url => request.url } }
      end

    else
      flash[:alert] = t('user.login_first')
      session[:user_return_to] = request.url

      respond_to do |format|
        format.html { redirect_to "/users/sign_in" }
        format.json { render json: { :message => flash[:error], :url => request.url } }
      end
    end
  end

  def set_timezone
    Time.zone = current_user.time_zone if current_user
  end

  def redirect_override?
    params[:redirect_to] ? true : false
  end

  def redirect_override
    params[:redirect_to]
  end

  def redirect
    to = params[:redirect_to]

    if redirect_override?
      temp_params = params
      temp_params.delete :redirect_to

      redirect_to to, temp_params
    end
  end
end
