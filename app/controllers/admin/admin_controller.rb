class Admin::AdminController < ApplicationController
  load_and_authorize_resource

  def index
    @admin_pages = Hash.new

    @admin_pages = {
        :users => {
            :title        => t('user.users'),
            :path         => admin_users_path,
            :description  => t('admin.manage_users')
        }
    }

    respond_to do |format|
      format.html
    end
  end
end