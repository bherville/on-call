class Admin::ManualOncallSchedulesController < ApplicationController
  # GET /manual_oncall_schedules
  # GET /manual_oncall_schedules.json
  def index
    @manual_oncall_schedules = ManualOncallSchedule.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @manual_oncall_schedules }
    end
  end

  # GET /manual_oncall_schedules/1
  # GET /manual_oncall_schedules/1.json
  def show
    @manual_oncall_schedule = ManualOncallSchedule.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @manual_oncall_schedule }
    end
  end

  # GET /manual_oncall_schedules/new
  # GET /manual_oncall_schedules/new.json
  def new
    @manual_oncall_schedule = ManualOncallSchedule.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @manual_oncall_schedule }
    end
  end

  # GET /manual_oncall_schedules/1/edit
  def edit
    @manual_oncall_schedule = ManualOncallSchedule.find(params[:id])
  end

  # POST /manual_oncall_schedules
  # POST /manual_oncall_schedules.json
  def create
    @manual_oncall_schedule = ManualOncallSchedule.new(params[:manual_oncall_schedule])
    @manual_oncall_schedule.created_by = current_user

    respond_to do |format|
      if @manual_oncall_schedule.save
        format.html { redirect_to @manual_oncall_schedule, notice: 'Manual oncall schedule was successfully created.' }
        format.json { render json: @manual_oncall_schedule, status: :created, location: @manual_oncall_schedule }
      else
        format.html { render action: "new" }
        format.json { render json: @manual_oncall_schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /manual_oncall_schedules/1
  # PUT /manual_oncall_schedules/1.json
  def update
    @manual_oncall_schedule = ManualOncallSchedule.find(params[:id])

    respond_to do |format|
      if @manual_oncall_schedule.update_attributes(params[:manual_oncall_schedule])
        format.html { redirect_to @manual_oncall_schedule, notice: 'Manual oncall schedule was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @manual_oncall_schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /manual_oncall_schedules/1
  # DELETE /manual_oncall_schedules/1.json
  def destroy
    @manual_oncall_schedule = ManualOncallSchedule.find(params[:id])
    @manual_oncall_schedule.destroy

    respond_to do |format|
      format.html { redirect_to manual_oncall_schedules_url }
      format.json { head :no_content }
    end
  end
end
