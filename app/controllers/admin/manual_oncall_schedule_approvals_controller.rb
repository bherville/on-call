class ManualOncallScheduleApprovalsController < ApplicationController
  # GET /manual_oncall_schedule_approvals
  # GET /manual_oncall_schedule_approvals.json
  def index
    @manual_oncall_schedule_approvals = ManualOncallScheduleApproval.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @manual_oncall_schedule_approvals }
    end
  end

  # GET /manual_oncall_schedule_approvals/1
  # GET /manual_oncall_schedule_approvals/1.json
  def show
    @manual_oncall_schedule_approval = ManualOncallScheduleApproval.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @manual_oncall_schedule_approval }
    end
  end

  # GET /manual_oncall_schedule_approvals/new
  # GET /manual_oncall_schedule_approvals/new.json
  def new
    @manual_oncall_schedule_approval = ManualOncallScheduleApproval.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @manual_oncall_schedule_approval }
    end
  end

  # GET /manual_oncall_schedule_approvals/1/edit
  def edit
    @manual_oncall_schedule_approval = ManualOncallScheduleApproval.find(params[:id])
  end

  # POST /manual_oncall_schedule_approvals
  # POST /manual_oncall_schedule_approvals.json
  def create
    @manual_oncall_schedule_approval = ManualOncallScheduleApproval.new(params[:manual_oncall_schedule_approval])

    respond_to do |format|
      if @manual_oncall_schedule_approval.save
        format.html { redirect_to @manual_oncall_schedule_approval, notice: 'Manual oncall schedule approval was successfully created.' }
        format.json { render json: @manual_oncall_schedule_approval, status: :created, location: @manual_oncall_schedule_approval }
      else
        format.html { render action: "new" }
        format.json { render json: @manual_oncall_schedule_approval.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /manual_oncall_schedule_approvals/1
  # PUT /manual_oncall_schedule_approvals/1.json
  def update
    @manual_oncall_schedule_approval = ManualOncallScheduleApproval.find(params[:id])

    respond_to do |format|
      if @manual_oncall_schedule_approval.update_attributes(params[:manual_oncall_schedule_approval])
        format.html { redirect_to @manual_oncall_schedule_approval, notice: 'Manual oncall schedule approval was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @manual_oncall_schedule_approval.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /manual_oncall_schedule_approvals/1
  # DELETE /manual_oncall_schedule_approvals/1.json
  def destroy
    @manual_oncall_schedule_approval = ManualOncallScheduleApproval.find(params[:id])
    @manual_oncall_schedule_approval.destroy

    respond_to do |format|
      format.html { redirect_to manual_oncall_schedule_approvals_url }
      format.json { head :no_content }
    end
  end
end
