class OncallSchedulesController < ApplicationController
  load_and_authorize_resource

  # GET /oncall_schedules
  def index
    @oncall_schedules = OncallSchedule.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /oncall_schedules/1
  def show
    @oncall_schedule = OncallSchedule.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /oncall_schedules/new
  def new
    @oncall_schedule = create_profiles OncallSchedule.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /oncall_schedules/1/edit
  def edit
    @oncall_schedule = create_profiles OncallSchedule.find(params[:id])
  end

  # POST /oncall_schedules
  def create
    @oncall_schedule = OncallSchedule.new(params[:oncall_schedule])
    @oncall_schedule.scheduler = current_user

    respond_to do |format|
      if @oncall_schedule.save
        format.html { redirect_to @oncall_schedule, notice: 'Oncall schedule was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /oncall_schedules/1
  def update
    @oncall_schedule = OncallSchedule.find(params[:id])
    profiles = @oncall_schedule.oncall_profiles

    respond_to do |format|
      if @oncall_schedule.update_attributes(params[:oncall_schedule])
        format.html { redirect_to @oncall_schedule, notice: 'Oncall schedule was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /oncall_schedules/1
  def destroy
    @oncall_schedule = OncallSchedule.find(params[:id])
    @oncall_schedule.destroy

    respond_to do |format|
      format.html { redirect_to oncall_schedules_url }
    end
  end

  private
  def create_profiles(oncall_schedule)
    profiles = []
    User.all.each do |user|
      next if !user.has_role?(:user)

      if oncall_schedule.id
        current = OncallProfile.find_by_user_id_and_oncall_schedule_id user.id, oncall_schedule.id
        next if current
      end


      profile = OncallProfile.new(
          :user => user,
          :oncall_schedule => oncall_schedule
      )
      profiles << profile
    end

    oncall_schedule.oncall_profiles << profiles
    oncall_schedule
  end
end