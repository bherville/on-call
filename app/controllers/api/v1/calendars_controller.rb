class API::V1::CalendarsController < ApplicationController
  def show
    @calendar = Calendar.get_calendar
  end

  def current_on_call
    calendar = Calendar.get_calendar
    overridden = calendar.schedule_overwritten

    if overridden && overridden.approval.approved?
      @oncall_profile = OncallProfile.new( :user => overridden.oncall_user)
    elsif calendar.oncall_schedule.present?
      @oncall_profile = calendar.oncall_schedule.current_on_call
    else
      @oncall_profile = nil
    end
  end

  def month_on_call_schedule
    calendar = Calendar.get_calendar

    @schedule_profiles = calendar.oncall_schedule.month_on_call
  end

  def on_call_users_by_date_range
    calendar = Calendar.get_calendar

    start_date = params[:start_date]
    end_date = params[:end_date]

    if (start_date && end_date)
      @schedule_profiles = calendar.oncall_schedule.users_between_dates Date.parse(start_date), Date.parse(end_date)
    else
      respond_to do |format|
        format.json {{ :error => 'You must supply both start_date and end_date', :status => 404 }}
      end
    end
  end
end
