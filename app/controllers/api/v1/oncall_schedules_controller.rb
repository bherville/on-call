class API::V1::OncallSchedulesController < ApplicationController
  def index
    @oncall_schedules = OncallSchedule.all
  end

  def show
    @oncall_schedule = OncallSchedule.find(params[:id])
  end
end
