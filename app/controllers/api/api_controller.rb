class API::APIController < ApplicationController
  # GET /api.json
  def show
    respond_to do |format|
      format.json { render json: { :versions => API_VERSIONS } }
    end
  end

  # GET /api/current_version.json
  def versions
    respond_to do |format|
      format.json { render json: { :versions => API_VERSIONS } }
    end
  end

  # GET /api/current_version.json
  def current_version
    respond_to do |format|
      format.json { render json: { :version => API_VERSIONS.last } }
    end
  end
end
