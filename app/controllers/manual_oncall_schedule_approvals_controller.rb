class ManualOncallScheduleApprovalsController < ApplicationController
  load_and_authorize_resource
  before_filter :can_approve_reject

  # GET /manual_oncall_schedule_approvals/1
  def show
    @manual_oncall_schedule_approval = ManualOncallScheduleApproval.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  def approve
    manual_oncall_schedule_approval = ManualOncallScheduleApproval.find(params[:id])

    if current_user.has_role? :scheduler
      success = manual_oncall_schedule_approval.admin_approve
    else
      success = manual_oncall_schedule_approval.approve
    end

    if (success)
      manual_oncall_schedule_approval.notify_users_status
      respond_to do |format|
        format.html { redirect_to manual_oncall_schedule_approval.manual_oncall_schedule, notice: 'Override request approved!' } unless redirect_override?
        format.html { redirect_to redirect_override, notice: 'Override request approved!' } if redirect_override?

      end
    else
      respond_to do |format|
        format.html { redirect_to manual_oncall_schedule_approval.manual_oncall_schedule, alert: 'Override request failed to be approved!' } unless redirect_override?
        format.html { redirect_to redirect_override, alert: 'Override request failed to be approved!' } if redirect_override?
      end
    end
  end

  def reject
    manual_oncall_schedule_approval = ManualOncallScheduleApproval.find(params[:id])

    if current_user.has_role? :scheduler
      success = manual_oncall_schedule_approval.admin_reject
    else
      success = manual_oncall_schedule_approval.reject
    end

    if (success)
      manual_oncall_schedule_approval.notify_users_status
      respond_to do |format|
        format.html { redirect_to manual_oncall_schedule_approval.manual_oncall_schedule, notice: 'Override request rejected!' } unless redirect_override?
        format.html { redirect_to redirect_override, notice: 'Override request rejected!' } if redirect_override?
      end
    else
      respond_to do |format|
        format.html { redirect_to manual_oncall_schedule_approval.manual_oncall_schedule, alert: 'Override request failed to be rejected!' } unless redirect_override?
        format.html { redirect_to redirect_override, alert: 'Override request failed to be rejected!' } if redirect_override?
      end
    end
  end

  private
  def can_approve_reject
    manual_oncall_schedule_approval = ManualOncallScheduleApproval.find(params[:id])

    if current_user == manual_oncall_schedule_approval.manual_oncall_schedule.overridden_on_call_user || current_user.has_role?(:scheduler)
      true
    else
      respond_to do |format|
        format.html { redirect_to manual_oncall_schedule_approval.manual_oncall_schedule, alert: 'You are not allowed to approve this override request!' } unless redirect_override?
        format.html { redirect_to redirect_override, alert: 'You are not allowed to approve this override request!' } if redirect_override?
      end
      false
    end
  end
end