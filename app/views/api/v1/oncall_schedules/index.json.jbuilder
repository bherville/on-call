json.array!(@oncall_schedules) do |schedule|
  json.array!(schedule.profiles_to_schedule) do |event|
    json.id event[:oncall_profile].id
    json.title event[:oncall_profile].user.full_name.nil? ? event[:oncall_profile].user.email : event[:oncall_profile].user.full_name
    json.start event[:start_date]
    json.end event[:end_date]
  end
end