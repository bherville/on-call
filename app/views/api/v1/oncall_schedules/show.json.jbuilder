json.array!(@oncall_schedule.profiles_to_schedule) do |event|
  json.id event[:oncall_profile].id
  json.title event[:oncall_profile].user.full_name.nil? ? event[:oncall_profile].user.email : event[:oncall_profile].user.full_name
  json.start event[:start_date]
  json.end event[:end_date]
end

json.array!(@oncall_schedule.manual_oncall_schedules) do |event|
  user = User.find(event.oncall_user_id)
  json.title user.full_name.nil? ? "OVERRIDE - #{user.email}" : "OVERRIDE - #{user.full_name}"
  json.start event.start_date
  json.end event.end_date
end