json.array!(@schedule_profiles) do |profile|
    json.fname profile[:oncall_profile].user.fname
    json.lname profile[:oncall_profile].user.lname
    json.email profile[:oncall_profile].user.email
    json.pager_address profile[:oncall_profile].user.pager_address
    json.start_date profile[:start_date]
    json.end_date profile[:end_date]
    json.time_zone profile[:start_date].zone
end