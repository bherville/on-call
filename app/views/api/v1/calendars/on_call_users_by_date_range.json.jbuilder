json.users(@schedule_profiles) do |profile|
  json.extract! profile[:oncall_profile].user, :fname, :lname, :name, :email, :pager_address unless @schedule_profiles.nil?
end
json.set! :count, @schedule_profiles.count.to_s
