module ManualOncallSchedulesHelper
  def on_call_schedule_users (oncall_schedule)
    oncall_schedule.scheduled_users.select { |u| u != current_user }
  end

  def approval_status (manual_schedule)
    if manual_schedule.approval.admin_rejected?
      t('manual_oncall_schedule_approvals.admin_rejected')
    elsif manual_schedule.approval.admin_approved?
        t('manual_oncall_schedule_approvals.admin_approved')
    elsif manual_schedule.approval.rejected?
      t('manual_oncall_schedule_approvals.rejected')
    else
      manual_schedule.approval.approved? ? t('manual_oncall_schedule_approvals.approved') : t('manual_oncall_schedule_approvals.pending_approval')
    end
  end

  def can_approve_reject(manual_oncall_schedule_approval)
    if current_user == manual_oncall_schedule_approval.manual_oncall_schedule.overridden_on_call_user || (current_user.has_role?(:scheduler) && current_user != manual_oncall_schedule_approval.manual_oncall_schedule.oncall_user)
      true
    else
      false
    end
  end

  def show_approve_link (manual_schedule)
    if manual_schedule.approval.approved?
      show_link = false
    else
      if current_user.has_role? :scheduler
        show_link = true
      elsif current_user.has_role?(:user) && current_user == manual_schedule.overridden_on_call_user
        if manual_schedule.approval.rejected? || manual_schedule.approval.approved?
          show_link = false
        else
          show_link = true
        end
      else
        show_link = false
      end
    end

    show_link
  end

  def show_reject_link (manual_schedule)
    if manual_schedule.approval.rejected?
      show_link = false
    else
      if current_user.has_role? :scheduler
        show_link = true
      elsif current_user.has_role?(:user) && current_user == manual_schedule.overridden_on_call_user
        if manual_schedule.approval.rejected? || manual_schedule.approval.approved?
          show_link = false
        else
          show_link = true
        end
      else
        show_link = false
      end
    end

    show_link
  end
end
