module ApplicationHelper
  def boolean_to_yes_no(boolean)
    case boolean
      when true
        'Yes'
      else
        'No'
      end
  end

  def branding_title
    title = ''

    title = "#{APP_CONFIG['branding']['title']} - " if APP_CONFIG['branding'] && APP_CONFIG['branding']['title']

    "#{title}#{t('oncall.oncall')}"
  end
end
