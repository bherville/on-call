require 'api_constraints'

Oncall::Application.routes.draw do
  resources :manual_oncall_schedule_approvals do
    member do
      get :approve
      get :reject
    end
  end
  resources :manual_oncall_schedules
  resource :calendars
  resources :oncall_schedules
  resource :user, only: [:show]


  devise_for :users, :controllers => { :registrations => 'registrations', :confirmations => 'confirmations', :omniauth_callbacks => 'users/omniauth_callbacks' }
  devise_scope :user do
    put '/confirm' => 'confirmations#confirm'
  end

  root to: 'calendars#show'


  namespace :admin do
    match '/' => 'admin#index'
    resources :users,  :controllers => { :users => 'admin/users'}
  end

  resource :api, :controller => 'api/api' do
    get :current_version
    get :versions
  end
  namespace :api, :defaults => {:format => :json} do
    #API
    api_routes = lambda {
      resources :oncall_schedules
      resource :calendars do
        collection do
          get :current_on_call
          get :month_on_call_schedule
          get :on_call_users_by_date_range
        end
      end
    }

    scope module: :v1, constraints: APIConstraints.new(version: 1, default: true) do
      api_routes.call
    end
  end
end
