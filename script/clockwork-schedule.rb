require 'clockwork'
require '../config/boot'
require '../config/environment'

module Clockwork
  handler do |job|
    puts "Running #{job}"
  end

  if Rails.env == 'development'
    check_every = 1.minute
  else
    check_every = 5.minutes
  end

  every(1.day, 'notify_oncall.send_overwrite_user_admin') {
    puts 'Sending all admins a reminder that the calendar has the overwrite user set.'
    Calendar.get_calendar.send_schedule_overwritten_to_admins if Calendar.get_calendar.schedule_overwritten
  }


  every(check_every, 'notify_oncall.send')  {
    if Calendar.get_calendar.oncall_schedule.nil?
      puts 'No OnCall Schedule configured. Skipping...'
    else
      puts 'OnCall Schedule configured.'
      if Calendar.get_calendar.schedule_overwritten && Calendar.get_calendar.schedule_overwritten.approval.approved?
        unless Calendar.get_calendar.overwrite_user_notification_sent?
          puts 'Notifying all users in the calendars associated schedule the overwrite user set.'
          Calendar.get_calendar.send_schedule_overwritten_to_users true
          puts 'Notifying mailing list in the calendars associated schedule the overwrite user set.'
          Calendar.get_calendar.send_schedule_overwritten_to_mailing_list
        end
      else
        # Get the current time in the zone of the OnCallSchedule's oncall_start_time.
        now = Time.now.in_time_zone(Calendar.get_calendar.oncall_schedule.oncall_start_time.time_zone.name)


        day_numbers = Hash[Date::DAYNAMES.map.with_index.to_a]
        scheduled_day = day_numbers[Calendar.get_calendar.oncall_schedule.oncall_start_day]

        puts "Today's Date: #{now}"
        puts "Today's Day of Week: #{now.wday}"

        if now.wday >= scheduled_day
          puts "Today is greater than or equal to #{scheduled_day}"
          #Schdule is set for today or earlier
          if now.strftime( "%H%M%S%N" ) >= Calendar.get_calendar.oncall_schedule.oncall_start_time.strftime( "%H%M%S%N" )
            puts "The time is greater than or equal to #{Calendar.get_calendar.oncall_schedule.oncall_start_time.strftime( "%H%M%S%N" )}"

            if Calendar.get_calendar.oncall_schedule.send_notify?
              puts 'Sending notification.'
              Calendar.get_calendar.oncall_schedule.notify_on_call
            else
              puts 'Notification already sent. Skipping...'
            end

          else
            puts "The scheduled time #{Calendar.get_calendar.oncall_schedule.oncall_start_time.strftime( "%H%M%S%N" )} has not been reached"
          end
        else
          puts "The scheduled week day #{scheduled_day} has not been reached"
        end
      end
    end
  }

  every(1.day, 'notify_all_monthly_on_call.send', :if => lambda { |t| t.day == 1 })  {
    if Calendar.get_calendar.oncall_schedule.nil?
      puts 'No OnCall Schedule configured. Skipping...'
    else
      if Calendar.get_calendar.oncall_schedule.send_monthly_notify?
        Calendar.get_calendar.oncall_schedule.notify_all_monthly_on_call
        puts 'Sending notification.'
      else
        puts 'Notification already sent. Skipping...'
      end
    end
  }
end