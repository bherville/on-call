# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

unless User.find_by_email('admin@example.com')
  require 'securerandom'
  password = SecureRandom.hex(8)

  admin = User.new(
      :email => 'admin@example.com',
      :password => password,
      :password_confirmation => password,
      :fname => 'Admin',
      :lname => 'User',
      :pager_address => '5555555555'
  )

  admin.roles << :admin
  admin.roles << :scheduler
  admin.skip_confirmation!
  admin.save!


  puts '################################'
  puts 'Administrator User Credentials'
  puts "Email Address: #{admin.email}"
  puts "Password: #{password}"
  puts '################################'
end

unless User.find_by_email('user@example.com')
  require 'securerandom'
  password = SecureRandom.hex(8)

  user = User.new(
      :email => 'user@example.com',
      :password => password,
      :password_confirmation => password,
      :fname => 'Test',
      :lname => 'User',
      :pager_address => '5555555555'
  )

  user.roles << :user
  user.skip_confirmation!
  user.save!


  puts '################################'
  puts "#{user.email} Credentials"
  puts "Email Address: #{user.email}"
  puts "Password: #{password}"
  puts '################################'
end

unless User.find_by_email('user1@example.com')
  require 'securerandom'
  password = SecureRandom.hex(8)

  user = User.new(
      :email => 'user1@example.com',
      :password => password,
      :password_confirmation => password,
      :fname => 'Test1',
      :lname => 'User',
      :pager_address => '5555555555'
  )

  user.roles << :user
  user.skip_confirmation!
  user.save!


  puts '################################'
  puts "#{user.email} Credentials"
  puts "Email Address: #{user.email}"
  puts "Password: #{password}"
  puts '################################'
end

unless User.find_by_email('user2@example.com')
  require 'securerandom'
  password = SecureRandom.hex(8)

  user = User.new(
      :email => 'user2@example.com',
      :password => password,
      :password_confirmation => password,
      :fname => 'Test2',
      :lname => 'User',
      :pager_address => '5555555555'
  )

  user.roles << :user
  user.skip_confirmation!
  user.save!


  puts '################################'
  puts "#{user.email} Credentials"
  puts "Email Address: #{user.email}"
  puts "Password: #{password}"
  puts '################################'
end