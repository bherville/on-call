# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20151001154321) do

  create_table "calendars", :force => true do |t|
    t.integer  "oncall_schedule_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "last_modified_by"
  end

  create_table "manual_oncall_schedule_approvals", :force => true do |t|
    t.boolean  "oncall_user_approval",             :default => false
    t.boolean  "overridden_on_call_user_approval", :default => false
    t.boolean  "admin_approval",                   :default => false
    t.boolean  "oncall_user_reject",               :default => false
    t.boolean  "overridden_on_call_user_reject",   :default => false
    t.boolean  "admin_reject",                     :default => false
    t.integer  "manual_oncall_schedule_id"
    t.integer  "admin_user_id"
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
  end

  create_table "manual_oncall_schedules", :force => true do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "oncall_user_id"
    t.integer  "overridden_on_call_user_id"
    t.integer  "created_by"
    t.integer  "oncall_schedule_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "oncall_profiles", :force => true do |t|
    t.integer  "user_id"
    t.integer  "oncall_schedule_id"
    t.integer  "oncall_position"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "oncall_schedules", :force => true do |t|
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.integer  "scheduler_id"
    t.integer  "last_notification_sent_to"
    t.text     "mailing_list"
    t.integer  "weeks"
    t.text     "name"
    t.datetime "oncall_start_time"
    t.string   "oncall_start_day"
    t.date     "last_monthly_notification_sent_at"
  end

  create_table "users", :force => true do |t|
    t.string   "time_zone"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0,  :null => false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        :default => 0,  :null => false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.integer  "roles_mask"
    t.text     "fname"
    t.text     "lname"
    t.text     "pager_address"
    t.integer  "notifications",          :default => 7
    t.string   "provider"
    t.integer  "uid"
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

end
