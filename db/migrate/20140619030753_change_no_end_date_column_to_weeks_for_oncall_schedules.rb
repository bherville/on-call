class ChangeNoEndDateColumnToWeeksForOncallSchedules < ActiveRecord::Migration
  def up
    remove_column :oncall_schedules, :no_end_date
    add_column :oncall_schedules, :weeks, :integer
  end

  def down
    remove_column :oncall_schedules, :weeks
    add_column :oncall_schedules, :no_end_date, :boolean
  end
end
