class CreateOncallProfiles < ActiveRecord::Migration
  def change
    create_table :oncall_profiles do |t|
      t.integer :user_id
      t.integer :oncall_schedule_id
      t.integer :oncall_position

      t.timestamps
    end
  end
end
