class AddMailingListColumnToOncallSchedules < ActiveRecord::Migration
  def change
    add_column :oncall_schedules, :mailing_list, :text
  end
end
