class AddNotificationsColumnToUsers < ActiveRecord::Migration
  def change
    add_column :users, :notifications, :integer, :default => 7
  end
end
