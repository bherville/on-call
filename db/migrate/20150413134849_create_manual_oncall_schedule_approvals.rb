class CreateManualOncallScheduleApprovals < ActiveRecord::Migration
  def change
    create_table :manual_oncall_schedule_approvals do |t|
      t.boolean :oncall_user_approval, :default => false
      t.boolean :overridden_on_call_user_approval, :default => false
      t.boolean :admin_approval, :default => false
      t.boolean :oncall_user_reject, :default => false
      t.boolean :overridden_on_call_user_reject, :default => false
      t.boolean :admin_reject, :default => false
      t.integer :manual_oncall_schedule_id
      t.integer :admin_user_id

      t.timestamps
    end
  end
end
