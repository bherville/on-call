class AddOnCallStartTimeColumnToOncallSchedules < ActiveRecord::Migration
  def change
    add_column :oncall_schedules, :oncall_start_time, :timestamp
  end
end
