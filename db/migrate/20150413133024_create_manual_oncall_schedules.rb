class CreateManualOncallSchedules < ActiveRecord::Migration
  def change
    create_table :manual_oncall_schedules do |t|
      t.date :start_date
      t.date :end_date
      t.integer :oncall_user_id
      t.integer :overridden_on_call_user_id
      t.integer :created_by
      t.integer :oncall_schedule_id

      t.timestamps
    end
  end
end
