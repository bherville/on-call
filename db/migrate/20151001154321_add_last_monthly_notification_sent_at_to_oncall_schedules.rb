class AddLastMonthlyNotificationSentAtToOncallSchedules < ActiveRecord::Migration
  def change
    add_column :oncall_schedules, :last_monthly_notification_sent_at, :date
  end
end
