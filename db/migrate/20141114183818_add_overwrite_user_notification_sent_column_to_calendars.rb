class AddOverwriteUserNotificationSentColumnToCalendars < ActiveRecord::Migration
  def change
    add_column :calendars, :overwrite_user_notification_sent, :boolean, :default => false
  end
end
