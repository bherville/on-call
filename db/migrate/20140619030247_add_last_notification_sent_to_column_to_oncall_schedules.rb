class AddLastNotificationSentToColumnToOncallSchedules < ActiveRecord::Migration
  def change
    add_column :oncall_schedules, :last_notification_sent_to, :integer
  end
end
