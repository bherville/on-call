class ChangePhoneNumberColumnToPagerAddressInUsers < ActiveRecord::Migration
  def up
    rename_column :users, :phone_number, :pager_address
  end

  def down
    rename_column :users, :pager_address, :phone_number
  end
end
