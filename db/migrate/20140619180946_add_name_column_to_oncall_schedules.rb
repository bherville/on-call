class AddNameColumnToOncallSchedules < ActiveRecord::Migration
  def change
    add_column :oncall_schedules, :name, :text
  end
end
