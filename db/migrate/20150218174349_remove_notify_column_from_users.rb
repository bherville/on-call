class RemoveNotifyColumnFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :notify
  end

  def down
    add_column :users, :notify, :boolean, :default => true
  end
end
