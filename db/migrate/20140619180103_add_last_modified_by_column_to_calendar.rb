class AddLastModifiedByColumnToCalendar < ActiveRecord::Migration
  def change
    add_column :calendars, :last_modified_by, :integer
  end
end
