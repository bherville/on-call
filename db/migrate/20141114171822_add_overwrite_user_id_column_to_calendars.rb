class AddOverwriteUserIdColumnToCalendars < ActiveRecord::Migration
  def change
    add_column :calendars, :overwrite_user_id, :integer
  end
end
