class AddSchedulerIdColumnToOnCallSchedule < ActiveRecord::Migration
  def change
    add_column :oncall_schedules, :scheduler_id, :integer
  end
end
