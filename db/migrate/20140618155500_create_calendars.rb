class CreateCalendars < ActiveRecord::Migration
  def change
    create_table :calendars do |t|
      t.integer :oncall_schedule_id

      t.timestamps
    end
  end
end
