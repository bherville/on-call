class AddOnCallStartDayColumnToOncallSchedules < ActiveRecord::Migration
  def change
    add_column :oncall_schedules, :oncall_start_day, :string
  end
end
