class RemoveOverwriteUserColumns < ActiveRecord::Migration
  def up
    remove_column :calendars, :overwrite_user_id
    remove_column :calendars, :overwrite_user_notification_sent
  end

  def down
    add_column :calendars, :overwrite_user_id, :integer
    add_column :calendars, :overwrite_user_notification_sent, :boolean, :default => false
  end
end
