class AddFnameAndLnameColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :fname, :text
    add_column :users, :lname, :text
  end
end
