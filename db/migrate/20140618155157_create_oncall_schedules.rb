class CreateOncallSchedules < ActiveRecord::Migration
  def change
    create_table :oncall_schedules do |t|
      t.timestamp :start_date
      t.timestamp :end_date

      t.timestamps
    end
  end
end
