class AddNoEndDateColumnToOncallSchedules < ActiveRecord::Migration
  def change
    add_column :oncall_schedules, :no_end_date, :boolean, :default => false
  end
end
